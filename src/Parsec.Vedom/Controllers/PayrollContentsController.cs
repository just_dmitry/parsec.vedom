﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;

    using Parsec.Vedom.Model;
    using Parsec.Vedom.Services;
    using Parsec.Vedom.ViewModels;

    [Authorize]
    public class PayrollContentsController : Controller
    {
        public const string Title = "Судьи";

        private const string ControllerName = "PayrollContents";

        private VedomDb db;

        private UserManager<User> userManager;

        private AccessManager accessManager;

        private PersonService personService;

        public PayrollContentsController(VedomDb db, UserManager<User> userManager, AccessManager accessManager, PersonService personService)
        {
            this.db = db;
            this.userManager = userManager;
            this.accessManager = accessManager;
            this.personService = personService;
        }

        #region Routing

        public static UrlActionContext IndexRoute(Guid tournamentId)
        {
            return new UrlActionContext { Action = nameof(Index), Controller = ControllerName, Values = new { tournamentId = tournamentId } };
        }

        public static UrlActionContext IndexRoute(Guid tournamentId, Guid payrollId)
        {
            return new UrlActionContext { Action = nameof(Index), Controller = ControllerName, Values = new { tournamentId = tournamentId, payrollId = payrollId } };
        }

        public static UrlActionContext CreateRoute(Guid tournamentId, Guid? payrollId = null)
        {
            return new UrlActionContext { Action = nameof(Create), Controller = ControllerName, Values = new { tournamentId = tournamentId, payrollId = payrollId } };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Edit), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        #endregion

        public async Task<ActionResult> Index(Guid tournamentId, Guid? payrollId)
        {
            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, tournamentId))
            {
                return Forbid();
            }

            ViewBag.Tournament = await db.Tournaments.AsNoTracking().SingleAsync(x => x.Id == tournamentId);
            ViewBag.Payrolls = await db.Payrolls.AsNoTracking().Where(x => x.TournamentId == tournamentId).OrderBy(x => x.Name).ToListAsync();

            var query = db.PayrollContents.AsNoTracking()
                .Include(x => x.Person)
                .Include(x => x.Payroll)
                .Include(x => x.TournamentJobInfo)
                .ThenInclude(x => x.JobTitle)
                .Where(x => x.Payroll.TournamentId == tournamentId);
            if (payrollId.HasValue)
            {
                query = query.Where(x => x.PayrollId == payrollId.Value);
            }

            var data = await query
                .OrderByDescending(x => x.TournamentJobInfo.JobTitle.SortOrder)
                .ThenBy(x => x.TournamentJobInfo.JobTitle.Name)
                .ThenBy(x => x.Person.FamilyName)
                .ThenBy(x => x.Person.FirstName)
                .ToListAsync();
            var model = data
                .GroupBy(x => x.Payroll)
                .OrderBy(x => x.Key.Name)
                .ToList();
            return View(model);
        }

        public async Task<ActionResult> Create(Guid tournamentId, Guid? payrollId)
        {
            var model = new PayrollContentViewModel
            {
                Id = Guid.NewGuid(),
                TournamentId = tournamentId,
                PayrollId = payrollId ?? Guid.Empty,
            };

            ViewBag.Payrolls = await db.Payrolls.AsNoTracking().Where(x => x.TournamentId == tournamentId).OrderBy(x => x.Title).ToListAsync();
            ViewBag.TournamentJobInfos = await db.TournamentJobInfos.AsNoTracking().Include(x => x.JobTitle).Where(x => x.TournamentId == tournamentId).OrderByDescending(x => x.JobTitle.SortOrder).ThenBy(x => x.JobTitle.Name).ToListAsync();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(PayrollContentViewModel model)
        {
            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.TournamentId))
            {
                return Forbid();
            }

            var payroll = await db.Payrolls.AsNoTracking().SingleAsync(x => x.Id == model.PayrollId);
            if (payroll.TournamentId != model.TournamentId)
            {
                ModelState.AddModelError(nameof(model.PayrollId), "Выбрана недопустимая Ведомость");
            }

            var jobInfo = await db.TournamentJobInfos.AsNoTracking().SingleAsync(x => x.Id == model.TournamentJobInfoId);
            if (jobInfo.TournamentId != model.TournamentId)
            {
                ModelState.AddModelError(nameof(model.TournamentJobInfoId), "Выбрана недопустимая должность");
            }

            if (!ModelState.IsValid)
            {
                ViewBag.Payrolls = await db.Payrolls.AsNoTracking().Where(x => x.TournamentId == model.TournamentId).OrderBy(x => x.Title).ToListAsync();
                ViewBag.TournamentJobInfos = await db.TournamentJobInfos.AsNoTracking().Include(x => x.JobTitle).Where(x => x.TournamentId == model.TournamentId).OrderByDescending(x => x.JobTitle.SortOrder).ThenBy(x => x.JobTitle.Name).ToListAsync();
                return View(model);
            }

            var person = await personService.SaveAsync(model, userId.ToString());

            var obj = new PayrollContent
            {
                Id = model.Id,
                PayrollId = model.PayrollId,
                TournamentJobInfoId = model.TournamentJobInfoId,
                PersonId = person.Id,
            };

            db.PayrollContents.Add(obj);

            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute(model.TournamentId, model.PayrollId)));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await db.PayrollContents.AsNoTracking()
                .Include(x => x.Payroll)
                .Include(x => x.Person)
                .SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.Payroll.TournamentId))
            {
                return Forbid();
            }

            ViewBag.Payrolls = await db.Payrolls.AsNoTracking().Where(x => x.TournamentId == model.Payroll.TournamentId).OrderBy(x => x.Title).ToListAsync();
            ViewBag.TournamentJobInfos = await db.TournamentJobInfos.AsNoTracking().Include(x => x.JobTitle).Where(x => x.TournamentId == model.Payroll.TournamentId).OrderByDescending(x => x.JobTitle.SortOrder).ThenBy(x => x.JobTitle.Name).ToListAsync();

            var viewModel = new PayrollContentViewModel
            {
                Id = id,
                TournamentId = model.Payroll.TournamentId,
                PayrollId = model.PayrollId,
                TournamentJobInfoId = model.TournamentJobInfoId ?? Guid.Empty,
                PersonId = model.PersonId ?? Guid.Empty,
            };
            model.Person.CopyTo(viewModel);

            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, PayrollContentViewModel model)
        {
            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.TournamentId))
            {
                return Forbid();
            }

            var payroll = await db.Payrolls.AsNoTracking().SingleAsync(x => x.Id == model.PayrollId);
            if (payroll.TournamentId != model.TournamentId)
            {
                ModelState.AddModelError(nameof(model.PayrollId), "Выбрана недопустимая Ведомость");
            }

            var jobInfo = await db.TournamentJobInfos.AsNoTracking().SingleAsync(x => x.Id == model.TournamentJobInfoId);
            if (jobInfo.TournamentId != model.TournamentId)
            {
                ModelState.AddModelError(nameof(model.TournamentJobInfoId), "Выбрана недопустимая должность");
            }

            if (!ModelState.IsValid)
            {
                ViewBag.Payrolls = await db.Payrolls.AsNoTracking().Where(x => x.TournamentId == model.TournamentId).OrderBy(x => x.Title).ToListAsync();
                ViewBag.TournamentJobInfos = await db.TournamentJobInfos.AsNoTracking().Include(x => x.JobTitle).Where(x => x.TournamentId == model.TournamentId).OrderByDescending(x => x.JobTitle.SortOrder).ThenBy(x => x.JobTitle.Name).ToListAsync();
                return View(model);
            }

            var person = await personService.SaveAsync(model, userId.ToString());

            var obj = new PayrollContent
            {
                Id = model.Id,
                PayrollId = model.PayrollId,
                TournamentJobInfoId = model.TournamentJobInfoId,
                PersonId = person.Id,
            };

            db.Entry(obj).State = EntityState.Modified;

            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute(model.TournamentId, model.PayrollId)));
        }

        [ValidateAntiForgeryToken]
        public async Task<List<AutocompleteItem>> Search(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                return null;
            }

            var words = query.Split(new[] { ' ' }, 3, StringSplitOptions.RemoveEmptyEntries);

            var dbQuery = db.Persons.AsNoTracking();

            if (words.Length == 3)
            {
                dbQuery = dbQuery.FromSql("SELECT * FROM [Persons] WHERE [FamilyName] LIKE {0} AND [FirstName] LIKE {1} AND [MiddleName] LIKE {2}", words[0] + "%", words[1] + "%", words[2] + "%");
            }
            else if (words.Length == 2)
            {
                dbQuery = dbQuery.FromSql("SELECT * FROM [Persons] WHERE [FamilyName] LIKE {0} AND [FirstName] LIKE {1}", words[0] + "%", words[1] + "%");
            }
            else
            {
                dbQuery = dbQuery.FromSql("SELECT * FROM [Persons] WHERE [FamilyName] LIKE {0}", words[0] + "%");
            }

            var list = await dbQuery.Where(x => x.ReplacedByPersonId == null)
                .OrderBy(x => x.FamilyName).ThenBy(x => x.FirstName)
                .Take(50)
                .ToListAsync();

            if (list.Count > 3)
            {
                return list.Select(x => new AutocompleteItem
                {
                    Id = x.Id.ToString(),
                    Label = x.FamilyName + " " + x.FirstName + " " + x.MiddleName,
                    Value = x,
                }).ToList();
            }

            // делаем запрос БЕЗ ограничения по истории
            list = await dbQuery
                .OrderBy(x => x.FamilyName).ThenBy(x => x.FirstName).ThenBy(x => x.ReplacedByPersonId)
                .ToListAsync();

            return list.Select(x => new AutocompleteItem
            {
                Id = x.Id.ToString(),
                Label = x.FamilyName + " " + x.FirstName + " " + x.MiddleName,
                Value = x,
            }).ToList();
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await db.PayrollContents
                .Include(x => x.Payroll)
                .Include(x => x.Person)
                .Include(x => x.TournamentJobInfo).ThenInclude(x => x.JobTitle)
                .AsNoTracking()
                .SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.Payroll.TournamentId))
            {
                return Forbid();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.PayrollContents.Include(x => x.Payroll).AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.Payroll.TournamentId))
            {
                return Forbid();
            }

            db.PayrollContents.Remove(model);
            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute(model.Payroll.TournamentId, model.PayrollId)));
        }

        [HttpPost]
        public async Task<ActionResult> RunCommand(string ids, Guid dest)
        {
            var guids = ids.Split(",").Select(x => Guid.Parse(x)).ToArray();
            var items = await db.PayrollContents.Include(x => x.Payroll).Where(x => guids.Contains(x.Id)).ToListAsync();
            var tournamentIds = items.Select(x => x.Payroll.TournamentId).Distinct().ToList();
            if (tournamentIds.Count > 1)
            {
                return BadRequest("Different tournaments");
            }

            var payroll = await db.Payrolls.AsNoTracking().FirstOrDefaultAsync(x => x.Id == dest);
            if (payroll == null)
            {
                return BadRequest("Dest not found");
            }

            if (payroll.TournamentId != tournamentIds[0])
            {
                return BadRequest("Tournaments mismatch");
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, tournamentIds[0]))
            {
                return Forbid();
            }

            foreach (var item in items)
            {
                item.PayrollId = dest;
            }

            await db.SaveChangesAsync();

            return Redirect(Url.Action(IndexRoute(payroll.TournamentId)));
        }
    }
}
