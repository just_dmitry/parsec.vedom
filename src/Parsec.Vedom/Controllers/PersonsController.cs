﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.Encodings.Web;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Vedom.Model;

    [Authorize(Startup.AccessPolicyAdmin)]
    public class PersonsController : Controller
    {
        public const string Title = "Персоны";

        private const string ControllerName = "Persons";

        private VedomDb db;

        public PersonsController(VedomDb db)
        {
            this.db = db;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext { Action = nameof(Index), Controller = ControllerName };
        }

        public static UrlActionContext ListRoute(string familyName)
        {
            return new UrlActionContext { Action = nameof(List), Controller = ControllerName, Values = new { familyName = familyName } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext UndeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Undelete), Controller = ControllerName, Values = new { Id = id } };
        }

        #endregion

        public async Task<ActionResult> Index()
        {
            var list = (await db.Persons
                .Select(x => new
                {
                    x.Id,
                    x.FamilyName,
                })
                .ToListAsync())
                .GroupBy(x => x.FamilyName)
                .Select(x => new KeyValuePair<string, long>(x.Key, x.Count()))
                .OrderBy(x => x.Key)
                .ToList();
            return View(list);
        }

        public async Task<ActionResult> List(string familyName)
        {
            if (string.IsNullOrEmpty(familyName))
            {
                return NotFound();
            }

            var list = await db.Persons
                .Where(x => x.FamilyName == familyName)
                .OrderBy(x => x.FirstName)
                .ThenBy(x => x.BirthYear)
                .ThenByDescending(x => x.CreatedAt)
                .ToListAsync();
            return View(list);
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var model = await db.Persons.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            ViewBag.List = await db.Persons
                .Where(x => x.ReplacedByPersonId == null)
                .Where(x => x.FamilyName == model.FamilyName && x.Id != model.Id)
                .OrderBy(x => x.FirstName)
                .ThenByDescending(x => x.CreatedAt)
                .ToListAsync();

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(Guid id, Guid replacedById)
        {
            var model = await db.Persons.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            model.ReplacedByPersonId = replacedById;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(ListRoute("XXX")).Replace("XXX", UrlEncoder.Default.Encode(model.FamilyName)));
        }

        public async Task<IActionResult> Undelete(Guid id)
        {
            var model = await db.Persons.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Undelete")]
        public async Task<IActionResult> UndeleteConfirm(Guid id)
        {
            var model = await db.Persons.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            if (model.ReplacedByPersonId != null)
            {
                model.ReplacedByPersonId = null;
                await db.SaveChangesAsync();
            }

            return Redirect(Url.Action(ListRoute("XXX")).Replace("XXX", UrlEncoder.Default.Encode(model.FamilyName)));
        }
    }
}
