﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;

    using Parsec.Vedom.Model;
    using Parsec.Vedom.Services;

    [Authorize]
    public class PayrollsController : Controller
    {
        public const string Title = "Ведомости";

        private const string ControllerName = "Payrolls";

        private VedomDb db;

        private UserManager<User> userManager;

        private AccessManager accessManager;

        public PayrollsController(VedomDb db, UserManager<User> userManager, AccessManager accessManager)
        {
            this.db = db;
            this.userManager = userManager;
            this.accessManager = accessManager;
        }

        #region Routing

        public static UrlActionContext CreateRoute(Guid tournamentId)
        {
            return new UrlActionContext { Action = nameof(Create), Controller = ControllerName, Values = new { tournamentId = tournamentId } };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Edit), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext PrintRoute(Guid id, string template)
        {
            return new UrlActionContext { Action = nameof(Print), Controller = ControllerName, Values = new { Id = id, template = template } };
        }

        public static UrlActionContext PrintAllRoute(Guid tournamentId, string template)
        {
            return new UrlActionContext { Action = nameof(PrintAll), Controller = ControllerName, Values = new { tournamentId = tournamentId, template = template } };
        }

        #endregion

        public async Task<ActionResult> Create(Guid tournamentId)
        {
            var tournament = await db.Tournaments.AsNoTracking().SingleOrDefaultAsync(x => x.Id == tournamentId);
            if (tournament == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, tournamentId))
            {
                return Forbid();
            }

            var model = new Payroll
            {
                Id = Guid.NewGuid(),
                TournamentId = tournamentId,
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(Guid tournamentId, Payroll model)
        {
            var tournament = await db.Tournaments.AsNoTracking().SingleOrDefaultAsync(x => x.Id == tournamentId);
            if (tournament == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, tournamentId))
            {
                return Forbid();
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.TournamentId = tournamentId;

            db.Entry(model).State = EntityState.Added;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(TournamentsController.DetailsRoute(tournamentId)));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await db.Payrolls.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.TournamentId))
            {
                return Forbid();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, Payroll model)
        {
            var model2 = await db.Payrolls.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model2 == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model2.TournamentId))
            {
                return Forbid();
            }

            model.TournamentId = model2.TournamentId;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(TournamentsController.DetailsRoute(model.TournamentId)));
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await db.Payrolls.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.TournamentId))
            {
                return Forbid();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.Payrolls.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.TournamentId))
            {
                return Forbid();
            }

            db.Entry(model).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(TournamentsController.DetailsRoute(model.TournamentId)));
        }

        public async Task<ActionResult> Print(Guid id, string template)
        {
            var obj = await db.Payrolls.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (obj == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, obj.TournamentId))
            {
                return Forbid();
            }

            var model = await GetPrintAsync(obj.TournamentId, id);

            return View("Print/" + template, model);
        }

        public async Task<ActionResult> PrintAll(Guid tournamentId, string template)
        {
            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, tournamentId))
            {
                return Forbid();
            }

            var ids = await db.Payrolls.Where(x => x.TournamentId == tournamentId && !x.IsDummy).Select(x => x.Id).ToArrayAsync();

            var model = await GetPrintAsync(tournamentId, ids);

            return View("Print/" + template, model);
        }

        protected async Task<List<IGrouping<Guid, PayrollContent>>> GetPrintAsync(Guid tournamentId, params Guid[] ids)
        {
            ViewBag.Signatures = await db.PayrollContents.AsNoTracking()
                .Include(x => x.Person)
                .Include(x => x.TournamentJobInfo)
                .ThenInclude(x => x.JobTitle)
                .Where(x => x.TournamentJobInfo.TournamentId == tournamentId)
                .OrderByDescending(x => x.TournamentJobInfo.JobTitle.SortOrder)
                .Take(2)
                .ToListAsync();

            ViewBag.Tournament = await db.Tournaments.SingleAsync(x => x.Id == tournamentId);

            var data = await db.PayrollContents.AsNoTracking()
                .Include(x => x.Payroll).ThenInclude(x => x.Tournament).ThenInclude(x => x.Company)
                .Include(x => x.Person)
                .Include(x => x.TournamentJobInfo).ThenInclude(x => x.JobTitle)
                .Where(x => ids.Contains(x.PayrollId))
                .OrderByDescending(x => x.TournamentJobInfo.JobTitle.SortOrder)
                .ThenBy(x => x.TournamentJobInfo.JobTitle.Name)
                .ThenBy(x => x.Person.FamilyName)
                .ThenBy(x => x.Person.FirstName)
                .ToListAsync();

            var model = data.GroupBy(x => x.PayrollId).ToList();

            return model;
        }
    }
}
