﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;

    using Parsec.Vedom.Model;

    [Authorize(Startup.AccessPolicyAdmin)]
    public class JobTitlesController : Controller
    {
        public const string Title = "Должности";

        private const string ControllerName = "JobTitles";

        private VedomDb db;

        public JobTitlesController(VedomDb db)
        {
            this.db = db;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext { Action = nameof(Index), Controller = ControllerName };
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionContext { Action = nameof(Create), Controller = ControllerName };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Edit), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        #endregion

        public async Task<ActionResult> Index()
        {
            var list = await db.JobTitles.AsNoTracking()
                .OrderByDescending(x => x.SortOrder).ThenBy(x => x.Name)
                .ToListAsync();
            return View(list);
        }

        public ActionResult Create()
        {
            var model = new JobTitle
            {
                Id = Guid.NewGuid(),
                SortOrder = 100,
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(JobTitle model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.JobTitles.Add(model);
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await db.JobTitles.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, JobTitle model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await db.JobTitles.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.JobTitles.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            db.JobTitles.Remove(model);
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
