﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Vedom.Model;

    [Authorize]
    public class HomeController : Controller
    {
        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext { Action = nameof(Index), Controller = "Home" };
        }

        public static UrlActionContext AboutRoute()
        {
            return new UrlActionContext { Action = nameof(About), Controller = "Home" };
        }

        public static UrlActionContext LogsRoute()
        {
            return new UrlActionContext { Action = nameof(Logs), Controller = "Home" };
        }

        [Route("")]
        public IActionResult Index()
        {
            return Redirect(Url.Action(TournamentsController.IndexRoute()));
        }

        [AllowAnonymous]
        [Route("/about")]
        public async Task<IActionResult> About([FromServices] VedomDb db)
        {
            var model = new List<Tuple<string, string>>();
            try
            {
                model.Add(Tuple.Create("DB Test", (await db.Tournaments.CountAsync()).ToString()));
                model.Add(Tuple.Create("MONITORING", "XS7QJCZRZEMSQFPIUU6F"));
            }
            catch (Exception ex)
            {
                model.Add(Tuple.Create("Exception", ex.Message));
            }

            return View(model);
        }

        [Route("/logs")]
        public IActionResult Logs()
        {
            return View(iflight.Logging.MemoryLogger.LogList);
        }
    }
}
