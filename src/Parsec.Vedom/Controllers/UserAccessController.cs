﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Vedom.Model;
    using Parsec.Vedom.Services;
    using RecurrentTasks;

    [Authorize(Startup.AccessPolicyAdmin)]
    public class UserAccessController : Controller
    {
        public const string Title = "Доступ пользователей";

        private const string ControllerName = "UserAccess";

        private VedomDb db;

        private AccessManager accessManager;

        private ITask<AccessManagerUpdaterTask> accessManagerUpdaterTask;

        public UserAccessController(VedomDb db, AccessManager accessManager, ITask<AccessManagerUpdaterTask> accessManagerUpdaterTask)
        {
            this.db = db;
            this.accessManager = accessManager;
            this.accessManagerUpdaterTask = accessManagerUpdaterTask;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext { Action = nameof(Index), Controller = ControllerName };
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionContext { Action = nameof(Create), Controller = ControllerName };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Edit), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        #endregion

        public async Task<ActionResult> Index()
        {
            var list = await db.UserAccess.AsNoTracking()
                .Include(x => x.User)
                .Include(x => x.Company)
                .Include(x => x.Tournament)
                .ToListAsync();
            list = list.OrderBy(x => x.User.Name).ThenBy(x => x.Company?.Name).ThenByDescending(x => x.Tournament?.StartDate).ToList();
            return View(list);
        }

        public async Task<ActionResult> Create()
        {
            var model = new UserAccess
            {
                Id = Guid.NewGuid(),
            };

            ViewBag.Users = await db.Users.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
            ViewBag.Companies = await db.Companies.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
            ViewBag.Tournaments = await db.Tournaments.AsNoTracking().OrderByDescending(x => x.StartDate).ToListAsync();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(UserAccess model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Users = await db.Users.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
                ViewBag.Companies = await db.Companies.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
                ViewBag.Tournaments = await db.Tournaments.AsNoTracking().OrderByDescending(x => x.StartDate).ToListAsync();

                return View(model);
            }

            db.UserAccess.Add(model);
            await db.SaveChangesAsync();

            accessManagerUpdaterTask.TryRunImmediately();

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await db.UserAccess.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            ViewBag.Users = await db.Users.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
            ViewBag.Companies = await db.Companies.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
            ViewBag.Tournaments = await db.Tournaments.AsNoTracking().OrderByDescending(x => x.StartDate).ToListAsync();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, UserAccess model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Users = await db.Users.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
                ViewBag.Companies = await db.Companies.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
                ViewBag.Tournaments = await db.Tournaments.AsNoTracking().OrderByDescending(x => x.StartDate).ToListAsync();

                return View(model);
            }

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            accessManagerUpdaterTask.TryRunImmediately();

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await db.UserAccess.AsNoTracking().Include(x => x.User).Include(x => x.Company).Include(x => x.Tournament).SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.UserAccess.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            db.UserAccess.Remove(model);
            await db.SaveChangesAsync();

            accessManagerUpdaterTask.TryRunImmediately();

            return RedirectToAction(nameof(Index));
        }
    }
}
