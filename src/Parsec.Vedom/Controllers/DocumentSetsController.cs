﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;

    using Parsec.Vedom.Model;

    [Authorize(Startup.AccessPolicyAdmin)]
    public class DocumentSetsController : Controller
    {
        public const string Title = "Наборы документов";

        private const string ControllerName = "DocumentSets";

        private VedomDb db;

        public DocumentSetsController(VedomDb db)
        {
            this.db = db;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext { Action = nameof(Index), Controller = ControllerName };
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionContext { Action = nameof(Create), Controller = ControllerName };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Edit), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        #endregion

        public async Task<ActionResult> Index()
        {
            var list = await db.DocumentSets.AsNoTracking()
                .OrderBy(x => x.Name)
                .ToListAsync();
            return View(list);
        }

        public ActionResult Create()
        {
            var model = new DocumentSet
            {
                Id = Guid.NewGuid(),
                RegisterTemplate = "RegisterDefault",
                ContractTemplate = "ContractDefault",
                TimesheetTemplate = "Timesheet",
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(DocumentSet model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.DocumentSets.Add(model);
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await db.DocumentSets.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, DocumentSet model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await db.DocumentSets.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.DocumentSets.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            db.DocumentSets.Remove(model);
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
