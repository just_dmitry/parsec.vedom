﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;

    using Parsec.Vedom.Model;
    using Parsec.Vedom.Services;

    [Authorize]
    public class TournamentJobInfosController : Controller
    {
        public const string Title = "Должности";

        private const string ControllerName = "TournamentJobInfos";

        private VedomDb db;

        private UserManager<User> userManager;

        private AccessManager accessManager;

        public TournamentJobInfosController(VedomDb db, UserManager<User> userManager, AccessManager accessManager)
        {
            this.db = db;
            this.userManager = userManager;
            this.accessManager = accessManager;
        }

        #region Routing

        public static UrlActionContext CreateRoute(Guid tournamentId)
        {
            return new UrlActionContext { Action = nameof(Create), Controller = ControllerName, Values = new { tournamentId = tournamentId } };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Edit), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        #endregion

        public async Task<ActionResult> Create(Guid tournamentId)
        {
            var tournament = await db.Tournaments.AsNoTracking().SingleOrDefaultAsync(x => x.Id == tournamentId);
            if (tournament == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, tournamentId))
            {
                return Forbid();
            }

            ViewBag.JobTitles = await db.JobTitles.AsNoTracking().OrderByDescending(x => x.SortOrder).ThenBy(x => x.Name).ToListAsync();

            var model = new TournamentJobInfo
            {
                Id = Guid.NewGuid(),
                PersonsCount = 1,
                TournamentId = tournamentId,
                StartDate = tournament.StartDate,
                EndDate = tournament.EndDate,
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(Guid tournamentId, TournamentJobInfo model)
        {
            var tournament = await db.Tournaments.AsNoTracking().SingleOrDefaultAsync(x => x.Id == tournamentId);
            if (tournament == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, tournamentId))
            {
                return Forbid();
            }

            if (!ModelState.IsValid)
            {
                ViewBag.JobTitles = await db.JobTitles.AsNoTracking().OrderByDescending(x => x.SortOrder).ThenBy(x => x.Name).ToListAsync();
                return View(model);
            }

            model.TournamentId = tournamentId;

            db.Entry(model).State = EntityState.Added;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(TournamentsController.DetailsRoute(tournamentId)));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await db.TournamentJobInfos.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.TournamentId))
            {
                return Forbid();
            }

            ViewBag.JobTitles = await db.JobTitles.AsNoTracking().OrderByDescending(x => x.SortOrder).ThenBy(x => x.Name).ToListAsync();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, TournamentJobInfo model)
        {
            var model2 = await db.TournamentJobInfos.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model2 == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model2.TournamentId))
            {
                return Forbid();
            }

            model.TournamentId = model2.TournamentId;

            if (!ModelState.IsValid)
            {
                ViewBag.JobTitles = await db.JobTitles.AsNoTracking().OrderByDescending(x => x.SortOrder).ThenBy(x => x.Name).ToListAsync();
                return View(model);
            }

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(TournamentsController.DetailsRoute(model2.TournamentId)));
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await db.TournamentJobInfos.AsNoTracking().Include(x => x.JobTitle).SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.TournamentId))
            {
                return Forbid();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.TournamentJobInfos.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.TournamentId))
            {
                return Forbid();
            }

            db.Entry(model).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return Redirect(Url.Action(TournamentsController.DetailsRoute(model.TournamentId)));
        }
    }
}
