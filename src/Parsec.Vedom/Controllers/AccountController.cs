﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Parsec.Vedom.Model;

    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private const string Name = "Account";

        public AccountController(
            VedomDb db,
            UserManager<Model.User> userManager,
            SignInManager<Model.User> signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            Db = db;
        }

        public UserManager<Model.User> UserManager { get; private set; }

        public SignInManager<Model.User> SignInManager { get; private set; }

        public VedomDb Db { get; private set; }

        #region Routes

        public static UrlActionContext LoginRoute(string returnUrl = null)
        {
            return new UrlActionContext { Action = nameof(Login), Controller = Name, Values = new { returnUrl = returnUrl } };
        }

        public static UrlActionContext LogoutRoute(string returnUrl = null)
        {
            return new UrlActionContext { Action = nameof(Logout), Controller = Name };
        }

        public static UrlActionContext ExternalLoginCallbackRoute(string returnUrl)
        {
            return new UrlActionContext
            {
                Action = nameof(ExternalLoginCallback),
                Controller = Name,
                Values = new { ReturnUrl = returnUrl },
            };
        }

        #endregion

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewBag.ReturnUrl = returnUrl;
            ViewBag.LoginProviders = (await SignInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await SignInManager.SignOutAsync();
            return Redirect(Url.Action(HomeController.IndexRoute()));
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            var redirectUrl = Url.Action(ExternalLoginCallbackRoute(returnUrl));
            var properties = SignInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null)
        {
            var info = await SignInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return Redirect(Url.Action(LoginRoute()));
            }

            // If the user does not have an account - deny access
            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            var user = await UserManager.FindByEmailAsync(email);
            if (user == null || user.IsArchived)
            {
                await SignInManager.SignOutAsync();
                return Forbid();
            }

            await SignInManager.SignInAsync(user, isPersistent: true);

            user = await Db.Users.SingleAsync(x => x.UserId == user.UserId);
            user.LastLogon = DateTimeOffset.Now;
            await Db.SaveChangesAsync();

            return RedirectToLocal(returnUrl);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult AccessDenied(string returnUrl = null)
        {
            return View();
        }

        #region Helpers

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return Redirect(Url.Action(HomeController.IndexRoute()));
            }
        }

        #endregion
    }
}