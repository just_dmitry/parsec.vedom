﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;

    using Parsec.Vedom.Model;

    [Authorize(Startup.AccessPolicyAdmin)]
    public class UsersController : Controller
    {
        public const string Title = "Пользователи";

        private const string ControllerName = "Users";

        private VedomDb db;

        public UsersController(VedomDb db)
        {
            this.db = db;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext { Action = nameof(Index), Controller = ControllerName };
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionContext { Action = nameof(Create), Controller = ControllerName };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Edit), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        #endregion

        public async Task<ActionResult> Index()
        {
            var list = await db.Users.AsNoTracking()
                .OrderByDescending(x => x.Name)
                .ToListAsync();
            return View(list);
        }

        public ActionResult Create()
        {
            var model = new User
            {
                UserId = Guid.NewGuid(),
                NormalizedName = "-",
                NormalizedEmail = "-",
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(User model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.NormalizedEmail = model.EMail.ToUpperInvariant();
            model.NormalizedName = model.Name.ToUpperInvariant();
            model.SecurityStamp = Guid.NewGuid().ToString();
            db.Users.Add(model);
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await db.Users.AsNoTracking().SingleOrDefaultAsync(x => x.UserId == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, User model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.NormalizedEmail = model.EMail.ToUpperInvariant();
            model.NormalizedName = model.Name.ToUpperInvariant();
            model.SecurityStamp = Guid.NewGuid().ToString();
            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await db.Users.AsNoTracking().SingleOrDefaultAsync(x => x.UserId == id);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.Users.SingleOrDefaultAsync(x => x.UserId == id);
            if (model == null)
            {
                return NotFound();
            }

            db.Users.Remove(model);
            await db.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
