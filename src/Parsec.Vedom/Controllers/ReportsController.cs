﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;

    using Parsec.Vedom.Model;
    using Parsec.Vedom.Services;

    [Authorize]
    public class ReportsController : Controller
    {
        public const string Title = "Отчеты";

        private const string ControllerName = "Reports";

        private VedomDb db;

        private UserManager<User> userManager;

        private AccessManager accessManager;

        public ReportsController(VedomDb db, UserManager<User> userManager, AccessManager accessManager)
        {
            this.db = db;
            this.userManager = userManager;
            this.accessManager = accessManager;
        }

        #region Routing

        public static UrlActionContext PersonListSelectorRoute()
        {
            return new UrlActionContext { Action = nameof(PersonListSelector), Controller = ControllerName };
        }

        public static UrlActionContext PersonListRoute(Guid companyId, int year)
        {
            return new UrlActionContext { Action = nameof(PersonList), Controller = ControllerName, Values = new { companyId = companyId, year = year } };
        }

        #endregion

        public async Task<IActionResult> PersonListSelector()
        {
            var userId = Guid.Parse(userManager.GetUserId(User));

            var allowedCompanies = await db.UserAccess.AsNoTracking()
                .Where(x => x.UserId == userId)
                .Select(x => x.Company)
                .ToDictionaryAsync(x => x.Id);

            var allowedCompanyIds = allowedCompanies.Keys.ToArray();

            var data1 = await db.Tournaments
                .Where(x => allowedCompanyIds.Contains(x.CompanyId))
                .Select(x => new { x.CompanyId, TournamentId = x.Id, StartYear = x.StartDate.Year, EndYear = x.EndDate.Year })
                .ToListAsync();

            var data2 = data1
                .Select(x => new { x.CompanyId, x.TournamentId, Year = x.StartYear })
                .Concat(data1
                    .Where(x => x.EndYear != x.StartYear)
                    .Select(x => new { x.CompanyId, x.TournamentId, Year = x.EndYear }));

            var data3 = data2
                .GroupBy(x => new { x.CompanyId, x.Year })
                .Select(x => new { x.Key.CompanyId, x.Key.Year, Count = x.Count() })
                .GroupBy(x => x.CompanyId)
                .Select(x => Tuple.Create(
                    allowedCompanies[x.Key],
                    x.OrderBy(y => y.Year).Select(y => Tuple.Create(y.Year, y.Count)).ToList()))
                .OrderBy(x => x.Item1.Name)
                .ToList();

            return View(data3);
        }

        public async Task<IActionResult> PersonList(Guid companyId, int year)
        {
            var userId = Guid.Parse(userManager.GetUserId(User));

            var hasAccess = await db.UserAccess.AnyAsync(x => x.CompanyId == companyId && x.UserId == userId);
            if (!hasAccess)
            {
                return Forbid();
            }

            ViewBag.Company = await db.Companies.SingleAsync(x => x.Id == companyId);
            ViewBag.Year = year;

            var notBefore = new DateTime(year, 1, 1);
            var notAfter = new DateTime(year, 12, 31);

            var tournaments = await db.Tournaments
                .Where(x => x.CompanyId == companyId)
                .Where(x => (x.StartDate >= notBefore && x.StartDate <= notAfter)
                            || (x.EndDate >= notBefore && x.EndDate <= notAfter))
                .Select(x => x.Id)
                .ToListAsync();

            var payrolls = await db.Payrolls
                .Where(x => !x.IsDummy && tournaments.Contains(x.TournamentId))
                .Select(x => x.Id)
                .ToListAsync();

            var persons = await db.PayrollContents
                .Where(x => payrolls.Contains(x.PayrollId))
                .Where(x => x.PersonId != null)
                .Select(x => x.PersonId.Value)
                .ToListAsync();

            var data = await db.Persons
                .Where(x => persons.Contains(x.Id))
                .Select(x => new { x.FamilyName, x.FirstName, x.MiddleName })
                .Distinct()
                .OrderBy(x => x.FamilyName)
                .ThenBy(x => x.FirstName)
                .ThenBy(x => x.MiddleName)
                .Select(x => Tuple.Create(x.FamilyName, x.FirstName, x.MiddleName))
                .ToListAsync();

            return View(data);
        }
    }
}