﻿namespace Parsec.Vedom.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Vedom.Model;
    using Parsec.Vedom.Services;
    using Parsec.Vedom.ViewModels;
    using RecurrentTasks;

    [Authorize]
    public class TournamentsController : Controller
    {
        public const string Title = "Соревнования";

        private const string ControllerName = "Tournaments";

        private VedomDb db;

        private UserManager<User> userManager;

        private AccessManager accessManager;

        private ITask<AccessManagerUpdaterTask> accessManagerUpdaterTask;

        public TournamentsController(VedomDb db, UserManager<User> userManager, AccessManager accessManager, ITask<AccessManagerUpdaterTask> accessManagerUpdaterTask)
        {
            this.db = db;
            this.userManager = userManager;
            this.accessManager = accessManager;
            this.accessManagerUpdaterTask = accessManagerUpdaterTask;
        }

        #region Routing

        public static UrlActionContext IndexRoute()
        {
            return new UrlActionContext { Action = nameof(Index), Controller = ControllerName };
        }

        public static UrlActionContext DetailsRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Details), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext CreateRoute()
        {
            return new UrlActionContext { Action = nameof(Create), Controller = ControllerName };
        }

        public static UrlActionContext CreateViaCopyRoute()
        {
            return new UrlActionContext { Action = nameof(CreateViaCopy), Controller = ControllerName };
        }

        public static UrlActionContext EditRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Edit), Controller = ControllerName, Values = new { Id = id } };
        }

        public static UrlActionContext DeleteRoute(Guid id)
        {
            return new UrlActionContext { Action = nameof(Delete), Controller = ControllerName, Values = new { Id = id } };
        }

        #endregion

        public async Task<ActionResult> Index()
        {
            var userId = Guid.Parse(userManager.GetUserId(User));

            var list = db.Tournaments
                .Include(x => x.Company)
                .Include(x => x.DocumentSet)
                .OrderByDescending(x => x.StartDate)
                .ThenBy(x => x.Name);
            var list2 = await accessManager.AllowedOnly(userId, list).ToListAsync();
            return View(list2);
        }

        [Authorize(Startup.AccessPolicyTournamentCreator)]
        public async Task<IActionResult> CreateViaCopy()
        {
            var model = new TournamentCreateViaCopy()
            {
                NewDate = DateTime.Now,
            };

            ViewBag.Tournaments = await db.Tournaments.AsNoTracking()
                .OrderByDescending(x => x.StartDate)
                .Take(100)
                .ToListAsync();

            return View(model);
        }

        [HttpPost]
        [Authorize(Startup.AccessPolicyTournamentCreator)]
        public async Task<IActionResult> CreateViaCopy(TournamentCreateViaCopy model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Tournaments = await db.Tournaments.AsNoTracking()
                    .OrderByDescending(x => x.StartDate)
                    .Take(100)
                    .ToListAsync();

                return View(model);
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, model.OldTournamnetId))
            {
                return Forbid();
            }

            var tournament = await db.Tournaments.AsNoTracking().SingleAsync(x => x.Id == model.OldTournamnetId);

            var timeAdd = model.NewDate.Subtract(tournament.StartDate);

            tournament.Id = Guid.NewGuid();
            tournament.StartDate += timeAdd;
            tournament.EndDate += timeAdd;
            tournament.Name = model.NewName;
            tournament.AgreementDate += timeAdd;
            db.Entry(tournament).State = EntityState.Added;

            var tournamentJobInfoMappings = new Dictionary<Guid, Guid>();

            var tournamentJobInfoes = await db.TournamentJobInfos.AsNoTracking().Where(x => x.TournamentId == model.OldTournamnetId).ToListAsync();
            foreach (var jobInfo in tournamentJobInfoes)
            {
                tournamentJobInfoMappings[jobInfo.Id] = Guid.NewGuid();

                jobInfo.Id = tournamentJobInfoMappings[jobInfo.Id];
                jobInfo.TournamentId = tournament.Id;
                jobInfo.StartDate += timeAdd;
                jobInfo.EndDate += timeAdd;
                db.Entry(jobInfo).State = EntityState.Added;
            }

            var payrollMappings = new Dictionary<Guid, Guid>();

            var payrolls = await db.Payrolls.AsNoTracking().Where(x => x.TournamentId == model.OldTournamnetId).ToListAsync();
            foreach (var payroll in payrolls)
            {
                payrollMappings[payroll.Id] = Guid.NewGuid();

                payroll.Id = payrollMappings[payroll.Id];
                payroll.TournamentId = tournament.Id;
                db.Entry(payroll).State = EntityState.Added;
            }

            var oldPayrollIds = payrollMappings.Keys.ToArray();
            var payrollContents = await db.PayrollContents.AsNoTracking().Where(x => oldPayrollIds.Contains(x.PayrollId)).ToListAsync();
            foreach (var pc in payrollContents)
            {
                pc.Id = Guid.NewGuid();
                pc.PayrollId = payrollMappings[pc.PayrollId];
                if (pc.TournamentJobInfoId != null)
                {
                    pc.TournamentJobInfoId = tournamentJobInfoMappings[pc.TournamentJobInfoId.Value];
                }

                db.Entry(pc).State = EntityState.Added;
            }

            await db.SaveChangesAsync();

            accessManagerUpdaterTask.TryRunImmediately();

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Startup.AccessPolicyTournamentCreator)]
        public async Task<ActionResult> Create()
        {
            var userId = Guid.Parse(userManager.GetUserId(User));
            ViewBag.Companies = await accessManager.AllowedOnly(userId, db.Companies.AsNoTracking()).OrderBy(x => x.Name).ToListAsync();
            ViewBag.DocumentSets = await db.DocumentSets.AsNoTracking().OrderBy(x => x.Name).ToListAsync();

            var model = new Tournament
            {
                Id = Guid.NewGuid(),
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
            };
            return View(model);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, id))
            {
                return Forbid();
            }

            var retVal = new TournamentDetailsViewModel();

            retVal.Tournament = await db.Tournaments.AsNoTracking()
                .Include(x => x.Company)
                .Include(x => x.DocumentSet)
                .SingleOrDefaultAsync(x => x.Id == id);
            if (retVal.Tournament == null)
            {
                return NotFound();
            }

            retVal.JobInfoes = await db.TournamentJobInfos.AsNoTracking()
                .Include(x => x.JobTitle)
                .Where(x => x.TournamentId == id)
                .OrderByDescending(x => x.JobTitle.SortOrder)
                .ThenBy(x => x.JobTitle.Name)
                .ToListAsync();

            retVal.Payrolls = await db.Payrolls
                .Where(x => x.TournamentId == id)
                .OrderBy(x => x.Name)
                .Select(x => new TournamentDetailsViewModel.PayrollInfo
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsDummy = x.IsDummy,
                })
                .ToListAsync();

            foreach (var payroll in retVal.Payrolls)
            {
                payroll.Count = await db.PayrollContents.CountAsync(x => x.PayrollId == payroll.Id && x.TournamentJobInfoId != null && x.Person != null);
            }

            return View(retVal);
        }

        [HttpPost]
        public async Task<ActionResult> Create(Tournament model)
        {
            var userId = Guid.Parse(userManager.GetUserId(User));

            if (!accessManager.CanAccessCompany(userId, model.CompanyId))
            {
                ModelState.AddModelError(nameof(model.CompanyId), "Вам не разрешено выбирать это значение");
            }

            if (!ModelState.IsValid)
            {
                ViewBag.Companies = await accessManager.AllowedOnly(userId, db.Companies.AsNoTracking(), model.CompanyId).OrderBy(x => x.Name).ToListAsync();
                ViewBag.DocumentSets = await db.DocumentSets.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
                return View(model);
            }

            db.Entry(model).State = EntityState.Added;
            await db.SaveChangesAsync();

            accessManagerUpdaterTask.TryRunImmediately();

            return Redirect(Url.Action(DetailsRoute(model.Id)));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await db.Tournaments.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, id))
            {
                return Forbid();
            }

            ViewBag.Companies = await accessManager.AllowedOnly(userId, db.Companies.AsNoTracking(), model.CompanyId).OrderBy(x => x.Name).ToListAsync();
            ViewBag.DocumentSets = await db.DocumentSets.AsNoTracking().OrderBy(x => x.Name).ToListAsync();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, Tournament model)
        {
            var model2 = await db.Tournaments.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
            if (model2 == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, id))
            {
                return Forbid();
            }

            model.Id = id;

            if (!accessManager.CanAccessCompany(userId, model.CompanyId))
            {
                ModelState.AddModelError(nameof(model.CompanyId), "Вам не разрешено выбирать это значение");
            }

            if (!ModelState.IsValid)
            {
                ViewBag.Companies = await accessManager.AllowedOnly(userId, db.Companies.AsNoTracking(), model.CompanyId).OrderBy(x => x.Name).ToListAsync();
                ViewBag.DocumentSets = await db.DocumentSets.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
                return View(model);
            }

            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();

            accessManagerUpdaterTask.TryRunImmediately();

            return Redirect(Url.Action(DetailsRoute(id)));
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var model = await db.Tournaments.AsNoTracking().Include(x => x.Company).Include(x => x.DocumentSet).SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, id))
            {
                return Forbid();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(Guid id)
        {
            var model = await db.Tournaments.SingleOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            var userId = Guid.Parse(userManager.GetUserId(User));
            if (!accessManager.CanAccessTournament(userId, id))
            {
                return Forbid();
            }

            db.Entry(model).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            accessManagerUpdaterTask.TryRunImmediately();

            return RedirectToAction(nameof(Index));
        }
    }
}
