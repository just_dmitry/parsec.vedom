﻿namespace Parsec.Vedom
{
    using System;
    using System.Globalization;
    using System.Security.Claims;
    using System.Text.Unicode;
    using Logging.ExceptionSender;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Parsec.Vedom.Model;
    using Parsec.Vedom.Services;

    public class Startup
    {
        public const string AccessPolicyAdmin = "Admin";
        public const string AccessPolicyTournamentCreator = "TournamentCreator";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Microsoft.Extensions.WebEncoders.WebEncoderOptions>(
               o =>
               {
                   o.TextEncoderSettings = new System.Text.Encodings.Web.TextEncoderSettings();
                   o.TextEncoderSettings.AllowRanges(new[] { UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic });
               });

            var connString = Configuration["ConnectionString"];
            services.AddDbContextPool<VedomDb>(
                o => o
                    .UseSqlServer(connString, so => so.EnableRetryOnFailure())
                    .ConfigureWarnings(wa => wa.Throw(RelationalEventId.QueryClientEvaluationWarning)));

            services.AddIdentity<User, UserRole>(options => options.User.AllowedUserNameCharacters = null)
                .AddUserStore<VedomDb>()
                .AddRoleStore<VedomDb>();

            services
                .AddAuthentication()
                .AddGoogle(options =>
                {
                    options.ClientId = Configuration["Google.OAuth:ClientId"];
                    options.ClientSecret = Configuration["Google.OAuth:ClientSecret"];

                    options.UserInformationEndpoint = "https://www.googleapis.com/oauth2/v2/userinfo";
                    options.ClaimActions.Clear();
                    options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
                    options.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                    options.ClaimActions.MapJsonKey(ClaimTypes.GivenName, "given_name");
                    options.ClaimActions.MapJsonKey(ClaimTypes.Surname, "family_name");
                    options.ClaimActions.MapJsonKey("urn:google:profile", "link");
                    options.ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
                    options.ClaimActions.MapJsonKey("urn:google:image", "picture");
                });

            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });

            services.Configure<RouteOptions>(options =>
            {
                options.LowercaseUrls = true;
            });

            services.Configure<AuthorizationOptions>(options =>
            {
                options.AddPolicy(AccessPolicyAdmin, policy => policy.RequireClaim("IsAdmin"));
                options.AddPolicy(AccessPolicyTournamentCreator, policy => policy.RequireClaim("IsTournamentCreator"));
            });

            services.AddTransient(typeof(BootstrapMvc.Mvc6.BootstrapHelper<>));
            services.AddExceptionSender<ExceptionSenderMailgunTask>();
            services.Configure<ExceptionSenderOptions>(Configuration.GetSection("ExceptionSender"));
            services.Configure<ExceptionSenderMailgunOptions>(Configuration.GetSection("ExceptionSender"));

            services.AddSingleton<AccessManager>();
            services.AddTask<AccessManagerUpdaterTask>(o => o.AutoStart(TimeSpan.FromHours(1)));
            services.AddScoped<PersonService>();
        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddMemory(Configuration.GetSection("Logging"));

            app.UseStatusCodePages();
            app.UseDeveloperExceptionPage();
            app.UseExceptionSender();

            var staticFileOptions = new StaticFileOptions
            {
                OnPrepareResponse = (context) =>
                {
                    context.Context.Response.Headers.Add("Cache-Control", "public, max-age=15552000"); // 180 days
                },
            };
            app.UseStaticFiles(staticFileOptions);

            app.UseAuthentication();

            var ruCulture = new CultureInfo("ru-RU");
            ruCulture.NumberFormat.CurrencyDecimalSeparator = ".";
            ruCulture.NumberFormat.NumberDecimalSeparator = ".";

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture(ruCulture, ruCulture),
                SupportedCultures = new[] { ruCulture },
                SupportedUICultures = new[] { ruCulture },
            });

            CultureInfo.DefaultThreadCurrentCulture = ruCulture;
            CultureInfo.DefaultThreadCurrentUICulture = ruCulture;

            app.UseMvcWithDefaultRoute();
        }
    }
}
