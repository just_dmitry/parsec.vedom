﻿namespace Parsec.Vedom.ViewModels
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class TournamentCreateViaCopy
    {
        [Required]
        public Guid OldTournamnetId { get; set; }

        [Required]
        public string NewName { get; set; }

        [Required]
        public DateTime NewDate { get; set; }
    }
}
