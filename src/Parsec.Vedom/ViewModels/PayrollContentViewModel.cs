﻿namespace Parsec.Vedom.ViewModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Parsec.Vedom.Model;

    public class PayrollContentViewModel : PersonInfo
    {
        public enum PersonUpdateMode : byte
        {
            ThisAndFuture = 0,
            ThisOnly = 1,
        }

        [Required]
        public Guid Id { get; set; }

        [Required]
        public Guid TournamentId { get; set; }

        [Required]
        public Guid PayrollId { get; set; }

        [Required]
        public Guid TournamentJobInfoId { get; set; }

        public Guid PersonId { get; set; }

        public PersonUpdateMode UpdateMode { get; set; }
    }
}
