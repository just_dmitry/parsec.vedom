﻿namespace Parsec.Vedom.ViewModels
{
    using System;
    using System.Collections.Generic;
    using BootstrapMvc.Lists;
    using Parsec.Vedom.Model;

    public class TournamentDetailsViewModel
    {
        public Tournament Tournament { get; set; }

        public List<TournamentJobInfo> JobInfoes { get; set; }

        public List<PayrollInfo> Payrolls { get; set; }

        public class PayrollInfo
        {
            public Guid Id { get; set; }

            public string Name { get; set; }

            public bool IsDummy { get; set; }

            public int Count { get; set; }
        }
    }
}
