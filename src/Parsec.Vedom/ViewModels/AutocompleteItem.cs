﻿namespace Parsec.Vedom.ViewModels
{
    using System;

    public class AutocompleteItem
    {
        public string Id { get; set; }

        public string Label { get; set; }

        public object Value { get; set; }
    }
}
