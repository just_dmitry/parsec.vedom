﻿namespace System
{
    using System.Security.Cryptography;

    public static class StringExtensions
    {
        private static readonly MD5 MD5HashProvider = MD5.Create();

        public static string GetMD5Hash(this string value)
        {
            var hashBytes = MD5HashProvider.ComputeHash(Text.Encoding.UTF8.GetBytes(value));
            return BitConverter.ToString(hashBytes).Replace("-", string.Empty).ToLowerInvariant();
        }
    }
}