﻿namespace BootstrapMvc
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using BootstrapMvc.Controls;
    using BootstrapMvc.Core;

    public static class BootstrapMvcExtensions
    {
        public static IItemWriter<T> Muted<T>(this IItemWriter<T> target)
            where T : Element
        {
            target.CssClass("text-muted");
            return target;
        }

        public static IItemWriter<T, TContent> Muted<T, TContent>(this IItemWriter<T, TContent> target)
            where T : ContentElement<TContent>
            where TContent : DisposableContent
        {
            target.CssClass("text-muted");
            return target;
        }

        public static IItemWriter<Select, SelectContent> Select<T>(
            this IAnyContentMarker contentHelper,
            IEnumerable<T> values,
            Func<T, object> keyFunction,
            Func<T, string> valueFunction)
        {
            var items = values.Select(x => contentHelper.SelectOption(keyFunction(x), valueFunction(x)));
            return contentHelper.Select(items);
        }
    }
}
