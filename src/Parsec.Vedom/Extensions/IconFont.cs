﻿namespace BootstrapMvc
{
    using System;
    using Microsoft.AspNetCore.Html;

    public static class IconFont
    {
        public static readonly HtmlString Edit = Icon("pencil");

        public static readonly HtmlString Add = Icon("plus");

        public static readonly HtmlString Remove = Icon("remove");

        public static readonly HtmlString List = Icon("list-alt");

        public static readonly HtmlString Print = Icon("print");

        public static readonly HtmlString FileOutline = Icon("file-o");

        public static readonly HtmlString File = Icon("file");

        public static readonly HtmlString FilesOutline = Icon("files-o");

        public static readonly HtmlString FileTextOutline = Icon("file-text-o");

        public static readonly HtmlString FileText = Icon("file-text");

        public static readonly HtmlString Check = Icon("check");

        public static readonly HtmlString ExclamationCircle = Icon("exclamation-circle");

        public static readonly HtmlString ExclamationTriangle = Icon("exclamation-triangle");

        private static HtmlString Icon(string className)
        {
            return new HtmlString($"<span class='icon-{className}'></span> ");
        }
    }
}
