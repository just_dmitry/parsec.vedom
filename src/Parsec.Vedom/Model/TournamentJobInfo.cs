﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TournamentJobInfo
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid TournamentId { get; set; }

        [ForeignKey("TournamentId")]
        public virtual Tournament Tournament { get; set; }

        [Required]
        public Guid JobTitleId { get; set; }

        [ForeignKey("JobTitleId")]
        public virtual JobTitle JobTitle { get; set; }

        public int PersonsCount { get; set; }

        public int DaysCount { get; set; }

        public decimal PayPerDay { get; set; }

        public decimal PayTotal { get; set; }

        public decimal Taxes { get; set; }

        public decimal HandOut { get; set; }

        [Display(Name = "Дата для договоров")]
        public DateTime? AgreementDate { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public virtual ICollection<PayrollContent> PayrollContents { get; set; }
    }
}
