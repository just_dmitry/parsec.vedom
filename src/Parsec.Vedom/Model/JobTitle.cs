﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class JobTitle
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string NamePublic { get; set; }

        [Required]
        public short SortOrder { get; set; }

        [Required]
        public short DaysBeforeDefault { get; set; }

        [Required]
        public short DaysAfterDefault { get; set; }
    }
}
