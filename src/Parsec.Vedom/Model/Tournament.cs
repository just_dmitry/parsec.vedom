﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Tournament
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Description("Количество рабочих дней")]
        public int WorkingDaysCount { get; set; }

        [Description("Всего начислено")]
        public decimal PayTotal { get; set; }

        [Description("Всего удержано налогов")]
        public decimal Taxes { get; set; }

        [MaxLength(1000)]
        public string TitleInstrumentalCase { get; set; }

        [Description("Всего к выплате")]
        public decimal HandOut { get; set; }

        [Display(Name = "Исполнитель")]
        [Required]
        public Guid CompanyId { get; set; }

        [Display(Name = "Исполнитель")]
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        [Display(Name = "Набор отчетных документов")]
        [Required]
        public Guid DocumentSetId { get; set; }

        [Display(Name = "Набор отчетных документов")]
        [ForeignKey("DocumentSetId")]
        public virtual DocumentSet DocumentSet { get; set; }

        [Display(Name = "Дата для договоров")]
        public DateTime? AgreementDate { get; set; }

        public bool IsLocked { get; set; }

        public virtual ICollection<TournamentJobInfo> JobInfos { get; set; }

        public virtual ICollection<Payroll> Payrolls { get; set; }
    }
}
