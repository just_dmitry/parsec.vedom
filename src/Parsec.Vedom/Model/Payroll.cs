﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Payroll
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid TournamentId { get; set; }

        [ForeignKey("TournamentId")]
        public virtual Tournament Tournament { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Title { get; set; }

        public bool IsDummy { get; set; }

        public virtual ICollection<PayrollContent> Contents { get; set; }
    }
}
