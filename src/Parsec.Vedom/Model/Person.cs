﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Person : PersonInfo
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(32)]
        public string HashValue { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [MaxLength(100)]
        public string CreatedBy { get; set; }

        public Guid? ReplacedByPersonId { get; set; }

        [ForeignKey("ReplacedByPersonId")]
        public virtual Person ReplacedByPerson { get; set; }

        public virtual ICollection<PayrollContent> PayrollContents { get; set; }
    }
}
