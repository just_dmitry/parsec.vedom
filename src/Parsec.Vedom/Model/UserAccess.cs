﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class UserAccess
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("User")]
        public Guid UserId { get; set; }

        public virtual User User { get; set; }

        [ForeignKey("Company")]
        public Guid? CompanyId { get; set; }

        public virtual Company Company { get; set; }

        [ForeignKey("Tournament")]
        public Guid? TournamentId { get; set; }

        public virtual Tournament Tournament { get; set; }
    }
}
