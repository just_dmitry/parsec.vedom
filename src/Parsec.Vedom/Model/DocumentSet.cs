﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class DocumentSet
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string ContractTemplate { get; set; }

        [MaxLength(50)]
        public string RegisterTemplate { get; set; }

        [MaxLength(50)]
        public string TimesheetTemplate { get; set; }
    }
}
