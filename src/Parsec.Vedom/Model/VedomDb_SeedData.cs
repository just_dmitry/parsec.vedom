﻿namespace Parsec.Vedom.Model
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public static class VedomDb_SeedData
    {
        public static void CreateSampleData(IServiceProvider serviceProvider, string adminEmail)
        {
            var db = serviceProvider.GetRequiredService<VedomDb>();

            db.Database.Migrate();

            // step 1: recreate admin
            var admin = db.Users.FirstOrDefaultAsync(x => x.EMail == adminEmail).Result;
            if (admin == null)
            {
                admin = new User
                {
                    UserId = Guid.NewGuid(),
                    Name = adminEmail,
                    NormalizedName = adminEmail.ToUpperInvariant(),
                    EMail = adminEmail,
                    NormalizedEmail = adminEmail.ToUpperInvariant(),
                    IsAdmin = true,
                };
                db.Users.Add(admin);
                db.SaveChanges();
            }
            else
            {
                if (!admin.IsAdmin || admin.IsArchived)
                {
                    admin.IsAdmin = true;
                    admin.IsArchived = false;
                    db.SaveChanges();
                }
            }
        }
    }
}
