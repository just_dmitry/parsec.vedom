﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class User
    {
        [Key]
        public Guid UserId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string NormalizedName { get; set; }

        [Required]
        [MaxLength(100)]
        public string EMail { get; set; }

        [Required]
        [MaxLength(100)]
        public string NormalizedEmail { get; set; }

        [Required]
        public bool IsTournamentCreator { get; set; }

        [Required]
        public bool IsAdmin { get; set; }

        [Required]
        public bool IsArchived { get; set; }

        public DateTimeOffset? LastLogon { get; set; }

        #region Identity: External login info

        [MaxLength(200)]
        public string LoginProvider { get; set; }

        [MaxLength(200)]
        public string ProviderDisplayName { get; set; }

        [MaxLength(200)]
        public string ProviderKey { get; set; }

        /// <summary>
        /// User security stamp value.
        /// See <see cref="http://stackoverflow.com/questions/37162791/"/> and <see cref="http://stackoverflow.com/questions/19487322/"/> for details.
        /// </summary>
        [MaxLength(100)]
        public string SecurityStamp { get; set; }

        #endregion
    }
}
