﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PayrollContent
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid PayrollId { get; set; }

        [ForeignKey("PayrollId")]
        public virtual Payroll Payroll { get; set; }

        public Guid? PersonId { get; set; }

        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }

        public Guid? TournamentJobInfoId { get; set; }

        [ForeignKey("TournamentJobInfoId")]
        public virtual TournamentJobInfo TournamentJobInfo { get; set; }
    }
}
