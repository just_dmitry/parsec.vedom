﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Company
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(1000)]
        public string NameFull { get; set; }

        [MaxLength(100)]
        public string NameShort { get; set; }

        [Required]
        [MaxLength(1000)]
        public string NameInDocument { get; set; }

        [Required]
        [MaxLength(100)]
        public string TaxNumber { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Address { get; set; }

        [Required]
        [MaxLength(1000)]
        public string BankInfo { get; set; }

        [MaxLength(100)]
        public string Phone { get; set; }

        [MaxLength(100)]
        public string ManagerName { get; set; }

        [MaxLength(100)]
        public string ManagerJobTitle { get; set; }

        public bool IsArchived { get; set; }
    }
}
