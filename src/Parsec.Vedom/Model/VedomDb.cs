﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.EntityFrameworkCore.Metadata.Internal;

    public partial class VedomDb : DbContext
    {
        public VedomDb(DbContextOptions<VedomDb> options)
            : base(options)
        {
            // Nothing
        }

        public DbSet<Company> Companies { get; set; }

        public DbSet<DocumentSet> DocumentSets { get; set; }

        public DbSet<Tournament> Tournaments { get; set; }

        public DbSet<JobTitle> JobTitles { get; set; }

        public DbSet<TournamentJobInfo> TournamentJobInfos { get; set; }

        public DbSet<Payroll> Payrolls { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<PayrollContent> PayrollContents { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserAccess> UserAccess { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>(t =>
            {
                t.HasIndex(x => x.Name).IsUnique();
                t.HasIndex(x => x.IsArchived);
            });

            modelBuilder.Entity<JobTitle>().HasIndex(x => x.Name).IsUnique();

            modelBuilder.Entity<Person>().HasIndex(x => new { x.ReplacedByPersonId, x.FamilyName, x.FirstName });

            modelBuilder.Entity<Tournament>(t =>
            {
                t.HasOne(x => x.Company).WithMany().OnDelete(DeleteBehavior.Restrict);
                t.HasIndex(x => x.Name).IsUnique();
                t.HasIndex(x => x.StartDate);
            });

            modelBuilder.Entity<TournamentJobInfo>().HasIndex(x => new { x.Id, x.JobTitleId }).IsUnique();

            modelBuilder.Entity<Person>(t =>
            {
                t.HasIndex(x => x.HashValue);
            });
        }
    }
}
