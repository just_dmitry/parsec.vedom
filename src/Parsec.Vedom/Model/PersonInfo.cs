﻿namespace Parsec.Vedom.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public abstract class PersonInfo
    {
        [Required]
        [MaxLength(100)]
        public string FamilyName { get; set; }

        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [MaxLength(100)]
        public string MiddleName { get; set; }

        [Range(1, 31)]
        public byte? BirthDay { get; set; }

        [Range(1, 12)]
        public byte? BirthMonth { get; set; }

        [Range(1900, 2900)]
        public short? BirthYear { get; set; }

        [MaxLength(10)]
        public string AddressIndex { get; set; }

        [MaxLength(1000)]
        public string Address { get; set; }

        [MaxLength(100)]
        public string DocumentNum { get; set; }

        public DateTime? DocumentDate { get; set; }

        [MaxLength(1000)]
        public string DocumentIssuedBy { get; set; }

        [MaxLength(100)]
        public string Phone { get; set; }

        [MaxLength(30)]
        public string TaxNumber { get; set; }

        [MaxLength(30)]
        public string PensionNumber { get; set; }

        [MaxLength(30)]
        public string InsuranceNumber { get; set; }

        public string GetBirthDate()
        {
            return (BirthDay?.ToString() ?? "__")
                + "."
                + (BirthMonth?.ToString("00") ?? "__")
                + "."
                + BirthYear.ToString();
        }

        public void CopyFrom(PersonInfo source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            FamilyName = source.FamilyName;
            FirstName = source.FirstName;
            MiddleName = source.MiddleName;
            BirthDay = source.BirthDay;
            BirthMonth = source.BirthMonth;
            BirthYear = source.BirthYear;
            AddressIndex = source.AddressIndex;
            Address = source.Address;
            DocumentNum = source.DocumentNum;
            DocumentDate = source.DocumentDate;
            DocumentIssuedBy = source.DocumentIssuedBy;
            Phone = source.Phone;
            TaxNumber = source.TaxNumber;
            PensionNumber = source.PensionNumber;
            InsuranceNumber = source.InsuranceNumber;
        }

        public void CopyTo(PersonInfo destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException(nameof(destination));
            }

            destination.CopyFrom(this);
        }

        public string ComputeHash()
        {
            var separator = "-";
            var combinedValue = string.Concat(new[]
            {
                FamilyName, separator,
                FirstName, separator,
                MiddleName, separator,
                BirthDay?.ToString(), separator,
                BirthMonth?.ToString(), separator,
                BirthYear?.ToString(), separator,
                AddressIndex, separator,
                Address, separator,
                DocumentNum, separator,
                DocumentDate?.ToString(), separator,
                DocumentIssuedBy, separator,
                Phone, separator,
                TaxNumber, separator,
                PensionNumber, separator,
                InsuranceNumber, separator,
            });
            return combinedValue.GetMD5Hash();
        }
    }
}
