﻿namespace Parsec.Vedom.Migrations
{
    using System;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class AgreementDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AgreementDate",
                table: "Tournaments",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "AgreementDate",
                table: "TournamentJobInfos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgreementDate",
                table: "Tournaments");

            migrationBuilder.DropColumn(
                name: "AgreementDate",
                table: "TournamentJobInfos");
        }
    }
}
