﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Parsec.Vedom.Model;

namespace Parsec.Vedom.Migrations
{
    [DbContext(typeof(VedomDb))]
    [Migration("20160905000029_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Parsec.Vedom.Model.Company", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("BankInfo")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<bool>("IsArchived");

                    b.Property<string>("ManagerJobTitle")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("ManagerName")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("NameFull")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("NameInDocument")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("Phone")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("TaxNumber")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.HasKey("Id");

                    b.HasIndex("IsArchived");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.DocumentSet", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ContractTemplate")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("RegisterTemplate")
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("Id");

                    b.ToTable("DocumentSets");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.JobTitle", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("DaysAfterDefault");

                    b.Property<short>("DaysBeforeDefault");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("NamePublic")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<short>("SortOrder");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("JobTitles");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.Payroll", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsDummy");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<Guid>("TournamentId");

                    b.HasKey("Id");

                    b.HasIndex("TournamentId");

                    b.ToTable("Payrolls");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.PayrollContent", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("PayrollId");

                    b.Property<Guid?>("PersonId");

                    b.Property<Guid?>("TournamentJobInfoId");

                    b.HasKey("Id");

                    b.HasIndex("PayrollId");

                    b.HasIndex("PersonId");

                    b.HasIndex("TournamentJobInfoId");

                    b.ToTable("PayrollContents");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.Person", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("AddressIndex")
                        .HasAnnotation("MaxLength", 10);

                    b.Property<byte?>("BirthDay");

                    b.Property<byte?>("BirthMonth");

                    b.Property<short?>("BirthYear");

                    b.Property<DateTimeOffset>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<DateTime?>("DocumentDate");

                    b.Property<string>("DocumentIssuedBy")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("DocumentNum")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("FamilyName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("HashValue")
                        .HasAnnotation("MaxLength", 32);

                    b.Property<string>("InsuranceNumber")
                        .HasAnnotation("MaxLength", 30);

                    b.Property<string>("MiddleName")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("PensionNumber")
                        .HasAnnotation("MaxLength", 30);

                    b.Property<string>("Phone")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<Guid?>("ReplacedByPersonId");

                    b.Property<string>("TaxNumber")
                        .HasAnnotation("MaxLength", 30);

                    b.HasKey("Id");

                    b.HasIndex("HashValue");

                    b.HasIndex("ReplacedByPersonId");

                    b.HasIndex("ReplacedByPersonId", "FamilyName", "FirstName");

                    b.ToTable("Persons");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.Tournament", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CompanyId");

                    b.Property<Guid>("DocumentSetId");

                    b.Property<DateTime>("EndDate");

                    b.Property<decimal>("HandOut");

                    b.Property<bool>("IsLocked");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<decimal>("PayTotal");

                    b.Property<DateTime>("StartDate");

                    b.Property<decimal>("Taxes");

                    b.Property<string>("TitleInstrumentalCase")
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<int>("WorkingDaysCount");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("DocumentSetId");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("StartDate");

                    b.ToTable("Tournaments");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.TournamentJobInfo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("DaysCount");

                    b.Property<DateTime>("EndDate");

                    b.Property<decimal>("HandOut");

                    b.Property<Guid>("JobTitleId");

                    b.Property<decimal>("PayPerDay");

                    b.Property<decimal>("PayTotal");

                    b.Property<int>("PersonsCount");

                    b.Property<DateTime>("StartDate");

                    b.Property<decimal>("Taxes");

                    b.Property<Guid>("TournamentId");

                    b.HasKey("Id");

                    b.HasIndex("JobTitleId");

                    b.HasIndex("TournamentId");

                    b.HasIndex("Id", "JobTitleId")
                        .IsUnique();

                    b.ToTable("TournamentJobInfos");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.User", b =>
                {
                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("EMail")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<bool>("IsAdmin");

                    b.Property<bool>("IsArchived");

                    b.Property<bool>("IsTournamentCreator");

                    b.Property<DateTimeOffset?>("LastLogon");

                    b.Property<string>("LoginProvider")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("NormalizedEmail")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("ProviderDisplayName")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("ProviderKey")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("SecurityStamp")
                        .HasAnnotation("MaxLength", 100);

                    b.HasKey("UserId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.UserAccess", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CompanyId");

                    b.Property<Guid?>("TournamentId");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("TournamentId");

                    b.HasIndex("UserId");

                    b.ToTable("UserAccess");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.Payroll", b =>
                {
                    b.HasOne("Parsec.Vedom.Model.Tournament", "Tournament")
                        .WithMany("Payrolls")
                        .HasForeignKey("TournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Vedom.Model.PayrollContent", b =>
                {
                    b.HasOne("Parsec.Vedom.Model.Payroll", "Payroll")
                        .WithMany("Contents")
                        .HasForeignKey("PayrollId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Vedom.Model.Person", "Person")
                        .WithMany("PayrollContents")
                        .HasForeignKey("PersonId");

                    b.HasOne("Parsec.Vedom.Model.TournamentJobInfo", "TournamentJobInfo")
                        .WithMany("PayrollContents")
                        .HasForeignKey("TournamentJobInfoId");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.Person", b =>
                {
                    b.HasOne("Parsec.Vedom.Model.Person", "ReplacedByPerson")
                        .WithMany()
                        .HasForeignKey("ReplacedByPersonId");
                });

            modelBuilder.Entity("Parsec.Vedom.Model.Tournament", b =>
                {
                    b.HasOne("Parsec.Vedom.Model.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("Parsec.Vedom.Model.DocumentSet", "DocumentSet")
                        .WithMany()
                        .HasForeignKey("DocumentSetId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Vedom.Model.TournamentJobInfo", b =>
                {
                    b.HasOne("Parsec.Vedom.Model.JobTitle", "JobTitle")
                        .WithMany()
                        .HasForeignKey("JobTitleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Parsec.Vedom.Model.Tournament", "Tournament")
                        .WithMany("JobInfos")
                        .HasForeignKey("TournamentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Parsec.Vedom.Model.UserAccess", b =>
                {
                    b.HasOne("Parsec.Vedom.Model.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("Parsec.Vedom.Model.Tournament", "Tournament")
                        .WithMany()
                        .HasForeignKey("TournamentId");

                    b.HasOne("Parsec.Vedom.Model.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
