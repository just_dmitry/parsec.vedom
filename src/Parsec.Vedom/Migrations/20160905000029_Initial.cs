﻿namespace Parsec.Vedom.Migrations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(maxLength: 1000, nullable: false),
                    BankInfo = table.Column<string>(maxLength: 1000, nullable: false),
                    IsArchived = table.Column<bool>(nullable: false),
                    ManagerJobTitle = table.Column<string>(maxLength: 100, nullable: true),
                    ManagerName = table.Column<string>(maxLength: 100, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    NameFull = table.Column<string>(maxLength: 1000, nullable: false),
                    NameInDocument = table.Column<string>(maxLength: 1000, nullable: false),
                    Phone = table.Column<string>(maxLength: 100, nullable: true),
                    TaxNumber = table.Column<string>(maxLength: 100, nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentSets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ContractTemplate = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    RegisterTemplate = table.Column<string>(maxLength: 50, nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentSets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobTitles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DaysAfterDefault = table.Column<short>(nullable: false),
                    DaysBeforeDefault = table.Column<short>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    NamePublic = table.Column<string>(maxLength: 200, nullable: true),
                    SortOrder = table.Column<short>(nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobTitles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(maxLength: 1000, nullable: true),
                    AddressIndex = table.Column<string>(maxLength: 10, nullable: true),
                    BirthDay = table.Column<byte>(nullable: true),
                    BirthMonth = table.Column<byte>(nullable: true),
                    BirthYear = table.Column<short>(nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    DocumentDate = table.Column<DateTime>(nullable: true),
                    DocumentIssuedBy = table.Column<string>(maxLength: 1000, nullable: true),
                    DocumentNum = table.Column<string>(maxLength: 100, nullable: true),
                    FamilyName = table.Column<string>(maxLength: 100, nullable: false),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    HashValue = table.Column<string>(maxLength: 32, nullable: true),
                    InsuranceNumber = table.Column<string>(maxLength: 30, nullable: true),
                    MiddleName = table.Column<string>(maxLength: 100, nullable: true),
                    PensionNumber = table.Column<string>(maxLength: 30, nullable: true),
                    Phone = table.Column<string>(maxLength: 100, nullable: true),
                    ReplacedByPersonId = table.Column<Guid>(nullable: true),
                    TaxNumber = table.Column<string>(maxLength: 30, nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Persons_Persons_ReplacedByPersonId",
                        column: x => x.ReplacedByPersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    EMail = table.Column<string>(maxLength: 100, nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false),
                    IsArchived = table.Column<bool>(nullable: false),
                    IsTournamentCreator = table.Column<bool>(nullable: false),
                    LastLogon = table.Column<DateTimeOffset>(nullable: true),
                    LoginProvider = table.Column<string>(maxLength: 200, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    NormalizedEmail = table.Column<string>(maxLength: 100, nullable: false),
                    NormalizedName = table.Column<string>(maxLength: 100, nullable: true),
                    ProviderDisplayName = table.Column<string>(maxLength: 200, nullable: true),
                    ProviderKey = table.Column<string>(maxLength: 200, nullable: true),
                    SecurityStamp = table.Column<string>(maxLength: 100, nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Tournaments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: false),
                    DocumentSetId = table.Column<Guid>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    HandOut = table.Column<decimal>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    PayTotal = table.Column<decimal>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    Taxes = table.Column<decimal>(nullable: false),
                    TitleInstrumentalCase = table.Column<string>(maxLength: 1000, nullable: true),
                    WorkingDaysCount = table.Column<int>(nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tournaments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tournaments_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tournaments_DocumentSets_DocumentSetId",
                        column: x => x.DocumentSetId,
                        principalTable: "DocumentSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payrolls",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDummy = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Title = table.Column<string>(maxLength: 1000, nullable: false),
                    TournamentId = table.Column<Guid>(nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payrolls", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payrolls_Tournaments_TournamentId",
                        column: x => x.TournamentId,
                        principalTable: "Tournaments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TournamentJobInfos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DaysCount = table.Column<int>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    HandOut = table.Column<decimal>(nullable: false),
                    JobTitleId = table.Column<Guid>(nullable: false),
                    PayPerDay = table.Column<decimal>(nullable: false),
                    PayTotal = table.Column<decimal>(nullable: false),
                    PersonsCount = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    Taxes = table.Column<decimal>(nullable: false),
                    TournamentId = table.Column<Guid>(nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TournamentJobInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TournamentJobInfos_JobTitles_JobTitleId",
                        column: x => x.JobTitleId,
                        principalTable: "JobTitles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TournamentJobInfos_Tournaments_TournamentId",
                        column: x => x.TournamentId,
                        principalTable: "Tournaments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserAccess",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: true),
                    TournamentId = table.Column<Guid>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccess", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserAccess_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAccess_Tournaments_TournamentId",
                        column: x => x.TournamentId,
                        principalTable: "Tournaments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAccess_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PayrollContents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PayrollId = table.Column<Guid>(nullable: false),
                    PersonId = table.Column<Guid>(nullable: true),
                    TournamentJobInfoId = table.Column<Guid>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayrollContents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PayrollContents_Payrolls_PayrollId",
                        column: x => x.PayrollId,
                        principalTable: "Payrolls",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PayrollContents_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PayrollContents_TournamentJobInfos_TournamentJobInfoId",
                        column: x => x.TournamentJobInfoId,
                        principalTable: "TournamentJobInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Companies_IsArchived",
                table: "Companies",
                column: "IsArchived");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_Name",
                table: "Companies",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobTitles_Name",
                table: "JobTitles",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Payrolls_TournamentId",
                table: "Payrolls",
                column: "TournamentId");

            migrationBuilder.CreateIndex(
                name: "IX_PayrollContents_PayrollId",
                table: "PayrollContents",
                column: "PayrollId");

            migrationBuilder.CreateIndex(
                name: "IX_PayrollContents_PersonId",
                table: "PayrollContents",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PayrollContents_TournamentJobInfoId",
                table: "PayrollContents",
                column: "TournamentJobInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_HashValue",
                table: "Persons",
                column: "HashValue");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_ReplacedByPersonId",
                table: "Persons",
                column: "ReplacedByPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_ReplacedByPersonId_FamilyName_FirstName",
                table: "Persons",
                columns: new[] { "ReplacedByPersonId", "FamilyName", "FirstName" });

            migrationBuilder.CreateIndex(
                name: "IX_Tournaments_CompanyId",
                table: "Tournaments",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Tournaments_DocumentSetId",
                table: "Tournaments",
                column: "DocumentSetId");

            migrationBuilder.CreateIndex(
                name: "IX_Tournaments_Name",
                table: "Tournaments",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tournaments_StartDate",
                table: "Tournaments",
                column: "StartDate");

            migrationBuilder.CreateIndex(
                name: "IX_TournamentJobInfos_JobTitleId",
                table: "TournamentJobInfos",
                column: "JobTitleId");

            migrationBuilder.CreateIndex(
                name: "IX_TournamentJobInfos_TournamentId",
                table: "TournamentJobInfos",
                column: "TournamentId");

            migrationBuilder.CreateIndex(
                name: "IX_TournamentJobInfos_Id_JobTitleId",
                table: "TournamentJobInfos",
                columns: new[] { "Id", "JobTitleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserAccess_CompanyId",
                table: "UserAccess",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccess_TournamentId",
                table: "UserAccess",
                column: "TournamentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccess_UserId",
                table: "UserAccess",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PayrollContents");

            migrationBuilder.DropTable(
                name: "UserAccess");

            migrationBuilder.DropTable(
                name: "Payrolls");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "TournamentJobInfos");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "JobTitles");

            migrationBuilder.DropTable(
                name: "Tournaments");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "DocumentSets");
        }
    }
}
