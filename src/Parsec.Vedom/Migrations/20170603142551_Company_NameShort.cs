﻿namespace Parsec.Vedom.Migrations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Company_NameShort : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Persons_ReplacedByPersonId",
                table: "Persons");

            migrationBuilder.AddColumn<string>(
                name: "NameShort",
                table: "Companies",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NameShort",
                table: "Companies");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_ReplacedByPersonId",
                table: "Persons",
                column: "ReplacedByPersonId");
        }
    }
}
