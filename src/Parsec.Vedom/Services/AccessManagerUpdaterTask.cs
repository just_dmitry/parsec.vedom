﻿namespace Parsec.Vedom.Services
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.DependencyInjection;
    using Parsec.Vedom.Model;
    using RecurrentTasks;

    public class AccessManagerUpdaterTask : IRunnable
    {
        private AccessManager accessManager;

        public AccessManagerUpdaterTask(AccessManager accessManager)
        {
            this.accessManager = accessManager;
        }

        public Task RunAsync(ITask currentTask, IServiceProvider scopeServiceProvider, CancellationToken cancellationToken)
        {
            return accessManager.ReloadAsync(scopeServiceProvider.GetRequiredService<VedomDb>());
        }
    }
}
