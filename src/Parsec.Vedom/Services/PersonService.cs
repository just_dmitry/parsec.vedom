﻿namespace Parsec.Vedom.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;

    using Parsec.Vedom.Model;
    using Parsec.Vedom.ViewModels;

    public class PersonService
    {
        private VedomDb db;

        public PersonService(VedomDb db)
        {
            this.db = db;
        }

        public async Task<Person> SaveAsync(PayrollContentViewModel model, string userId)
        {
            var hash = model.ComputeHash();

            var existingList = await db.Persons.Where(x => x.HashValue == hash).ToListAsync();

            var existing = existingList
                .Where(x => x.FamilyName == model.FamilyName)
                .Where(x => x.FirstName == model.FirstName)
                .Where(x => x.MiddleName == model.MiddleName)
                .Where(x => x.BirthYear == model.BirthYear)
                .FirstOrDefault();

            if (existing == null)
            {
                existing = new Person
                {
                    Id = Guid.NewGuid(),
                    HashValue = hash,
                    CreatedAt = DateTimeOffset.Now,
                    CreatedBy = userId,
                };
                existing.CopyFrom(model);

                db.Persons.Add(existing);
            }
            else if (model.UpdateMode == PayrollContentViewModel.PersonUpdateMode.ThisAndFuture)
            {
                existing.ReplacedByPersonId = null;
            }

            if (model.PersonId != Guid.Empty)
            {
                var previous = await db.Persons.SingleOrDefaultAsync(x => x.Id == model.PersonId);

                switch (model.UpdateMode)
                {
                    case PayrollContentViewModel.PersonUpdateMode.ThisOnly:
                        existing.ReplacedByPersonId = previous.Id;
                        break;
                    case PayrollContentViewModel.PersonUpdateMode.ThisAndFuture:
                        previous.ReplacedByPersonId = existing.Id;
                        break;
                }
            }

            return existing;
        }
    }
}
