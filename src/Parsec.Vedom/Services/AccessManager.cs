﻿namespace Parsec.Vedom.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Parsec.Vedom.Model;

    public class AccessManager
    {
        private List<UserAccess> userAccessList;

        private Guid[] adminUserIds;

        private Dictionary<Guid, Guid> tournamentCompanyMapping;

        public AccessManager()
        {
            userAccessList = new List<UserAccess>();
            adminUserIds = new Guid[0];
            tournamentCompanyMapping = new Dictionary<Guid, Guid>();
        }

        public async Task ReloadAsync(VedomDb db)
        {
            userAccessList = await db.UserAccess.AsNoTracking().ToListAsync();
            adminUserIds = await db.Users.Where(x => x.IsAdmin).Select(x => x.UserId).ToArrayAsync();
            tournamentCompanyMapping = await db.Tournaments.ToDictionaryAsync(x => x.Id, x => x.CompanyId);
        }

        public bool CanAccessTournament(Guid userId, Guid tournamentId)
        {
            if (adminUserIds.Contains(userId))
            {
                return true;
            }

            if (!tournamentCompanyMapping.ContainsKey(tournamentId))
            {
                return false;
            }

            var companyId = tournamentCompanyMapping[tournamentId];

            return userAccessList
                .Where(x => x.UserId == userId)
                .Any(x => x.TournamentId == tournamentId || x.CompanyId == companyId);
        }

        public bool CanAccessCompany(Guid userId, Guid companyId)
        {
            if (adminUserIds.Contains(userId))
            {
                return true;
            }

            return userAccessList
                .Where(x => x.UserId == userId)
                .Any(x => x.CompanyId == companyId);
        }

        public IQueryable<Company> AllowedOnly(Guid userId, IQueryable<Company> query)
        {
            return AllowedOnly(userId, query, Guid.Empty);
        }

        public IQueryable<Company> AllowedOnly(Guid userId, IQueryable<Company> query, Guid currentValue)
        {
            if (adminUserIds.Contains(userId))
            {
                return query;
            }

            var companies = userAccessList
                .Where(x => x.UserId == userId)
                .Where(x => x.CompanyId.HasValue)
                .Select(x => x.CompanyId.Value)
                .ToArray();

            return query
                .Where(x => companies.Contains(x.Id) || x.Id == currentValue);
        }

        public IQueryable<Tournament> AllowedOnly(Guid userId, IQueryable<Tournament> query)
        {
            return AllowedOnly(userId, query, Guid.Empty);
        }

        public IQueryable<Tournament> AllowedOnly(Guid userId, IQueryable<Tournament> query, Guid currentValue)
        {
            if (adminUserIds.Contains(userId))
            {
                return query;
            }

            var tournaments = userAccessList
               .Where(x => x.UserId == userId)
               .Where(x => x.TournamentId.HasValue)
               .Select(x => x.TournamentId.Value)
               .ToArray();
            var companies = userAccessList
                .Where(x => x.UserId == userId)
                .Where(x => x.CompanyId.HasValue)
                .Select(x => x.CompanyId.Value)
                .ToArray();

            return query
                .Where(x => x.Id == currentValue || tournaments.Contains(x.Id) || companies.Contains(x.CompanyId));
        }
    }
}
